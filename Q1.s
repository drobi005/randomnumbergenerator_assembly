#Create a program that will blink all of the LEDs on/off every 1s
.equ STAT, 0x10002000
.equ JTAG, 0x10001000
.equ ST, 350		#ST=Start_Timer
.equ KEY1, 0x10000050
.equ LEDS, 0x10000010
.global _start
.text
_start:
		movia r16, JTAG
_begin:
		movia r2, STAT		#move timer base address
		movia r3, ST		#move start-timer
		movia r5, LEDS
		stwio r3, 8(r2)		#set the default start timer(low)
		srli r3, r3, 16		#shift left 16 bit to get active bits in high-section
		stwio r3, 12(r2)	#set dault start timer(high)
		movi r4, 0b0110		#move 4(binary 100) into r4 - value for starting the timer
		stwio r6, 0(r5)		#turn LEDS on at the start
		
		stwio r4, 4(r2)	#Start the timer
		movia r8, KEY1
		
_keypressed: ldwio r11, 0(r8)	#load pushbuttons
			 bne r11, r0, _check	#if key1 is pressed(r11=/=0) then goto _check
			 br _keypressed	
_check: 
loop:	ldwio r11, 0(r8)		#load pushbuttons
		beq r11, r0, _snapshot	#if key1 is not pressed(r11==0) then goto _snapshot
		br loop					#keep looping until key1 is not pressed
_back:	ldwio r7, 0(r2)		
		andi r7, r7, 0b01		#Mask
		beq r7, r0, _back
		br _keypressed
_snapshot:
		stwio r2, 16(r2)		#store timer in registers[top and bottom sections]
		ldwio r9, 16(r2)		#load bottom 16bit
		andi r10, r9, 0b111		#Mask to get the 3 right most bits
		addi r10, r10, 48		#convert decimal value r10 to ascii value
		stb r10, 0(r16)			#write r10 value to terminal
		br _back
.data
MSG:
.asciz "\nRandom number: "